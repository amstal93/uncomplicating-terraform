variable "environment" {
  type        = string
  description = "The environment"
  default     = "staging"
}

variable "tags" {
  type        = map
  description = "The maps of tags of the instance"
  default = {
    "name"        = "MyServer"
    "environment" = "staging"
  }
}

variable "disks" {
  type = list(object({
    name = string, 
    size = number, 
    type = string
  }))
  default = [
    {
      name = "/dev/sdg"
      size = 5
      type = "gp2"
    }
  ]
}